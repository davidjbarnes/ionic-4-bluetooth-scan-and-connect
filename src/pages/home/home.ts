import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, Platform, NavParams } from 'ionic-angular';
import { BLE } from '@ionic-native/ble';

@Component({
  templateUrl: 'device-details.html',
})
export class NavigationDetailsPage {
  device: any;
  msg: string;
  connected: boolean;

  constructor(params: NavParams,
              private ble: BLE,
              private change: ChangeDetectorRef) {

    this.device = params.data.item;
    this.connect();
  }

  connect() {
    this.ble.connect(this.device.id).subscribe( data => {
      this.connected = true;
      this.change.detectChanges();

      setInterval(() => {
        this.getSignal();
        }, 1000);
    }, error => {
      this.connected = false;
      this.msg = error.toString();
      this.change.detectChanges();
    });
  }

  getSignal() {
    if(this.connected) {
      this.ble.readRSSI(this.device.id).then(data => {
        this.msg = "Signal: ".concat(data);
        this.change.detectChanges();
      });
    }
  }

  disconnect() {
    if(this.connected) {
      this.ble.disconnect(this.device.id).then(response => {
        this.connected = false;
        this.change.detectChanges();
      });
    }
  }
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  bluetoothStatus: string;
  scanning: boolean;
  device: any[] = [];

  constructor(private platform: Platform,
              private change: ChangeDetectorRef,
              public navCtrl: NavController,
              private ble: BLE) {

    this.device = [];

    this.platform.ready().then(() => {
      this.ble.startStateNotifications().subscribe( (state) => {
        this.bluetoothStatus = state;

        setTimeout(() => {
          this.change.detectChanges();
        }, 1000);
      });
    });
  }

  scan() {
    this.device = [];
    this.scanning = true;

    this.ble.startScan([]).subscribe((device) => {
        this.device.push(device);

        setTimeout(() => {
          this.change.detectChanges();
        }, 1000);
      },
      error => console.error(error)
    );

    setTimeout(() => {
      this.scanning = false;
      this.ble.stopScan();
      this.change.detectChanges();
    }, 5000)
  }

  itemSelected (item) {
    this.navCtrl.push(NavigationDetailsPage, { item: item });
  }
}
